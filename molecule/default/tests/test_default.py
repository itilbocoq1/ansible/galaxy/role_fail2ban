"""Role testing files using testinfra."""


def test_hosts_file(host):
    """Validate /etc/hosts file."""
    f = host.file("/etc/hosts")

    assert f.exists
    assert f.user == "root"
    assert f.group == "root"

def test_packages(host):
    pkg = host.package("fail2ban")
    assert pkg.is_installed
    # assert pkg.version.startswith("2.7")

def test_group(host):
    grp = host.group("root")
    assert grp.exists
